<?php
/**
 * @file
 * Euleo Übersetzungsbüro functions
 */


/**
 * Implements the callback.
 */
function euleo_callback() {
  $client = new EuleoClient();
  $client->connect();

  if (variable_get('euleo_install_token')) {
    $client->install(variable_get('euleo_install_token'));
  }
  else {
    $rows = $client->getRows();

    if (is_array($rows) && count($rows) > 0) {
      $confirm_ids = $client->callback($rows);

      if ($confirm_ids) {
        $client->confirmDelivery($confirm_ids);
      }
    }
  }

  echo 'callback response';
  exit();
}

/**
 * Implements the configuration page.
 */
function euleo_install_view() {
  $customer = variable_get('euleo_customer');
  $usercode = variable_get('euleo_usercode');

  try {
    $client = new EuleoCms($customer, $usercode, 'drupal');
  }
  catch (Exception $e) {
    variable_del('euleo_customer');
    variable_del('euleo_usercode');
  }

  ob_start();

  if (!empty($_POST['register'])) {
    variable_del('euleo_customer');
    variable_del('euleo_usercode');

    $cmsroot = $GLOBALS['base_root'] . base_path();
    $return_url = $cmsroot . '/admin/euleo/config';

    $token = $client->getRegisterToken($cmsroot, $return_url);

    variable_set('euleo_install_token', $token);

    $link = 'https://www.euleo.com/registercms/' . $token;

    $html = '<meta http-equiv="refresh" content="0; url=' . htmlspecialchars($link) . '" />';
  }
  else {
    $connected = FALSE;
    if (variable_get('euleo_usercode')) {
      $customer = $client->getCustomerData();
      if ($customer) {
        $connected = TRUE;
      }
    }

    $variables = array(
      'connected' => $connected,
      'customer' => $customer,
    );

    $html = theme('euleo_install', $variables);
  }

  return $html;
}

/**
 * Returns the HTML of the shopping cart.
 *
 * @param array $variables
 *   Contains the customer and connected
 */
function theme_euleo_install($variables) {
  $customer = $variables['customer'];
  $connected = $variables['connected'];

  ob_start();
  ?>
    <h1><?php echo t('Connect your Drupal installation with Euleo'); ?></h1>
  <?php
  if ($connected) {
    ?>
      <div class="updated">
        <?php echo t('You are currently connected as:'); ?>
        <b><?php echo $customer['emailaddress']; ?> (<?php echo (!empty($customer['company'])) ? $customer['company'] : ($customer['firstname'] . ' ' . $customer['lastname']); ?>)</b>
      </div>

      <p><?php echo t('If you want to use a different Euleo-User, click the button below!'); ?></p>
      <form action="<?php echo $_SERVER['REQUEST_URI'];?>" method="post">
        <input type="submit" name="register" value="<?php echo t('Re-connect with Euleo') ?>" />
      </form>
    <?php
  }
  else {
    ?>
      <p><?php echo t('You are only one step away from using Euleo for translating your website.'); ?></p>
      <p><?php echo t('Click the button below to connect your website with Euleo!'); ?></p>
      <form action="<?php echo $_SERVER['REQUEST_URI'];?>" method="post">
        <input type="submit" name="register" value="<?php echo t('Connect with Euleo') ?>" />
      </form>
    <?php
  }

  return ob_get_clean();
}


/**
 * Implements hook_permission().
 */
function euleo_permission() {
  return array(
    'access euleo' => array(
      'title' => t('Translate nodes using Euleo'),
    ),
  );
}

/**
 * Determine if a user is a manager for a language.
 *
 * (or any language if none is specified)
 *
 * @param string $lang_code
 *   with the language code. If no value is provided the
 *   a check will be made to see if they manage any language.
 *
 * @param object $account
 *   Drupal user object.
 *
 * @return bool
 *   this is a copy of translation_overview_is_manager
 */
function euleo_is_manager($lang_code = NULL, $account = NULL) {
  if (empty($lang_code)) {
    foreach (locale_language_list() as $lang_code => $language) {
      if (user_access('manage ' . check_plain($lang_code) . ' translation overview priorities', $account)) {
        return TRUE;
      }
    }
    return FALSE;
  }

  return user_access('manage ' . check_plain($lang_code) . ' translation overview priorities', $account);
}

/**
 * Get a list of the node types that have translation support enabled.
 *
 * @return array
 *   of with node types as keys and their names as values.
 *   this is a copy of translation_overview_node_types()
 */
function euleo_node_types() {
  $types = array();
  foreach (node_type_get_names() as $type => $name) {
    if (translation_supported_type($type)) {
      $types[$type] = $name;
    }
  }
  return $types;
}

/**
 * Implements the translation view.
 *
 * @return string
 *   HTML code
 */
function euleo_translation_view() {
  $params = drupal_get_query_parameters();

  drupal_add_css(drupal_get_path('module', 'euleo') . '/euleo.css');
  drupal_add_js(drupal_get_path('module', 'euleo') . '/euleo.js');

  // FIXME: get this from database.
  $default_language = 'de';

  $languages = locale_language_list();
  if (variable_get('euleo_customer') && variable_get('euleo_usercode')) {
    try {
      $client = new EuleoClient();
      $client->connect();

      $result = $client->setLanguages($languages);

      $unsupported = FALSE;
      if (isset($result['unsupported'])) {
        $unsupported = $result['unsupported'];
      }

      $cart = $client->getCart();

      // Bail if there are no translatable nodes.
      if (count(euleo_node_types()) == 0) {
        drupal_set_message(t('There are no translatable node types on this site.'), 'error');
        return '';
      }

      if (!empty($params['action'])) {
        if (empty($params['code'])) {
          drupal_set_message(t('No code supplied for action'), 'error');

          return '';
        }

        if (empty($params['dstlang'])) {
          drupal_set_message(t('No dstlang supplied for action'), 'error');

          return '';
        }
      }

      if (!empty($params['action']) && $params['action'] == 'add') {
        list ($what, $id) = explode('_', $params['code']);

        $row = array();
        $row['id'] = $what . '_' . $id;
        $row['code'] = $row['id'];
        $row['srclang'] = $default_language;
        $row['languages'] = $params['dstlang'];
        $row['fields'] = array();

        if ($what == 'node') {
          if (!empty($cart['request2languages']) && !empty($cart['request2languages'][$params['code']])) {
            $client->addLanguage($params['code'], $params['dstlang']);
          }
          else {
            $node = node_load($id);

            $row['label'] = t('Node') . ' #' . $id;

            $title = $row['label'];

            if ($node->title) {
              $title = $node->title;
            }

            if ($node->language) {
              $row['srclang'] = $node->language;
            }

            $row['description'] = $title;
            $row['title'] = $title;

            $row['url'] = url('node/' . $id, array(
              'absolute' => TRUE,
            ));

            foreach ($node as $key => $fields) {
              if ($key != 'body' && $key != 'title' && substr($key, 0, 6) != 'field_') {
                continue;
              }

              if (is_string($fields)) {
                $row['fields'][$key]['value'] = $fields;
                $row['fields'][$key]['label'] = $key;
                $row['fields'][$key]['type'] = 'textarea';
              }
              else {
                foreach ((array) $fields as $field_lang => $field) {
                  foreach ($field as $index => $data) {
                    if (isset($data['value'])) {
                      $subrow = array();
                      $subrow['code'] = $key . '||' . $field_lang . '||' . $index;
                      $subrow['id'] = $subrow['code'];

                      $subrow['fields'][$key]['value'] = $data['value'];
                      $subrow['fields'][$key]['label'] = $key;
                      $subrow['fields'][$key]['type'] = 'textarea';

                      if (in_array($data['format'], array(
                        'filtered_html',
                        'full_html',
                      ))) {
                        $subrow['fields'][$key]['type'] = 'richtextarea';
                        $subrow['fields'][$key]['value'] = nl2br($subrow['fields'][$key]['value']);
                      }

                      $row['rows'][] = $subrow;
                    }
                  }
                }
              }
            }

            $client->addRow($row);
            $client->sendRows();
          }
        }

        $cart = $client->getCart();
      }
      elseif (!empty($params['action']) && $params['action'] == 'remove') {
        $client->removeLanguage($params['code'], $params['dstlang']);

        $cart = $client->getCart();
      }

      $query = new SelectQuery('node', 'n', Database::getConnection());

      $query->addField('n', 'nid');
      $query->addField('n', 'title');
      $query->addField('n', 'type');
      $query->addField('n', 'created');
      $query->where("(n.nid = n.tnid OR n.tnid = 0) AND n.language <> '' AND n.language IS NOT NULL");

      $rows = array();
      $result = $query->execute();

      foreach ($result as $node) {
        $node = node_load($node->nid);

        $translations = (array) translation_node_get_translations($node->tnid);

        $row = array(
          array(
            'data' => l(translation_overview_trimmed_title($node), 'node/' . $node->nid . '/', array(
              'attributes' => array(
                'title' => check_plain($node->title),
              ),
            )),
          ),
          array(
            'data' => check_plain($node->type),
          ),
          array(
            'data' => format_date($node->created, 'custom', 'j M Y'),
          ),
        );

        foreach ($languages as $code => $lang) {
          $translation_node = empty($translations[$code]->nid) ? NULL : node_load($translations[$code]->nid);

          $state = euleo_get_translation_state($node, $translation_node, $code);

          $row[] = array(
            'data' => euleo_get_status_html($code, $node, $state),
          );
          $row[] = array(
            'data' => euleo_get_action_html($code, $node, $unsupported, $cart, $default_language),
          );
        }

        $rows['node_' . $node->nid] = $row;
      }

      $cart_html = theme('euleo_getcart', array('client' => $client));

      $header = array(
        array(
          'data' => t('Title'),
        ),
        array(
          'data' => t('Type'),
        ),
        array(
          'data' => t('Created'),
        ),
      );

      foreach ($languages as $code => $lang) {
        $header[] = array(
          'data' => '<img src="' . base_path() . drupal_get_path('module', 'euleo') . '/flags/' . htmlspecialchars($code) . '.png" alt="' . htmlspecialchars($lang) . '" />',
          'colspan' => 2,
        );
      }

      $themed = theme('table', array(
        'header' => $header,
        'rows' => $rows,
        'attributes' => array(
          'class' => array(
            'trov',
          ),
        ),
      ));

      return $cart_html . $themed;
    }
    catch (Exception $e) {
      return $e->getMessage();
    }
  }
  else {
    return t('Euleo is not yet configured. Go to configuration page and connect with your Euleo account!');
  }
}

/**
 * Returns the HTML of the shopping cart.
 *
 * @param array $variables
 *   Contains the EuleoClient object
 */
function theme_euleo_getcart($variables) {
  $client = $variables['client'];

  $cart = $client->getCart();

  $icon_dir = base_path() . drupal_get_path('module', 'euleo') . '/images';

  ob_start();
  if (isset($cart['allLanguages']) && $cart['allLanguages']) {
    ?>
      <h1><?php echo t('Cart'); ?></h1>
      <table class="shoppingcart" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <th><?php echo t('Language Combinations'); ?></th>
          <td>
            <ul class="allLanguages">
    <?php
    foreach ($cart['allLanguages'] as $language) {
      foreach ($language as $lang) {
        ?>
              <li>
                <?php echo $lang['count']; ?> <?php echo t('Texts'); ?> <?php echo $lang['title'] . ': <b>' . $lang['price'] . '</b>'; ?>
              </li>
        <?php
      }
    }
    ?>
            </ul>
          </td>
        </tr>
        <?php
    if ($cart['basePrices']) {
      ?>
        <tr>
          <th><?php echo t('Base prices'); ?></th>
          <td>
            <ul class="basePrices">
              <?php
      foreach ($cart['basePrices'] as $language) {
        foreach ($language as $lang) {
          ?>
                    <li>
                      <?php echo $lang['title']; ?>: <b><?php echo $lang['price']; ?></b>
                    </li>
          <?php
        }
      }
      ?>
            </ul>
          </td>
        </tr>
      <?php
    }

    if ($cart['deliveryTime']) {
      ?>
        <tr>
          <th><?php echo t('Delivery date'); ?></th>
          <td><span><?php echo format_date($cart['deliveryTime'], 'custom', 'd. F Y H:i'); ?></span></td>
        </tr>
      <?php
    }

    if ($cart['totalPrice']) {
      ?>
        <tr>
          <th><?php echo t('Net price'); ?>:</th>
          <td>
            <div class="cart_price">
              <div class="cart_priceBox">
                <span id="price"><?php echo $cart['totalPrice']; ?></span>
              </div>
            </div>
          </td>
        </tr>
      <?php
    }
    ?>
        <tr>
          <th colspan="2">
            <div class="showCart">
              <a href="<?php echo $client->startEuleoWebsite(); ?>" onclick="window.open(this.href, 'euleo'); return FALSE;">
                <img src="<?php echo $icon_dir; ?>/showCart.png" alt="" title="<?php echo t('Show cart'); ?>" />
                <span><?php echo t('Show cart'); ?></span>
              </a>
            </div>
          </th>
        </tr>
      </table>
    <?php
  }
  else {
    ?>
      <h2><?php echo t('Cart empty'); ?></h2>
    <?php
  }

  return ob_get_clean();
}

/**
 * Returns the status HTML.
 *
 * @param string $lang_code
 *   Language code in ISO-639-1
 * @param array $entry
 *   translateable row
 * @param string $state
 *   can be original, outofdate, current, missing
 */
function euleo_get_status_html($lang_code, $entry, $state) {
  ob_start();
  $icon_dir = euleo_get_icon_dir();

  switch ($state) {
    case 'original':
      $title = t('original');
      break;

    case 'outofdate':
      $title = t('outofdate');
      break;

    case 'current':
      $title = t('current');
      break;

    case 'missing':
      $title = t('missing');
      break;
  }

  ?>
    <img src="<?php echo $icon_dir; ?>/<?php echo $state; ?>.png" alt="" title="<?php echo $title; ?>" />
  <?php

  return ob_get_clean();
}

/**
 * Returns the action HTML.
 *
 * @param string $lang_code
 *   Language code in ISO-639-1
 * @param array $entry
 *   translateable row
 * @param array $unsupported
 *   unsupported languages
 * @param object $cart
 *   shopping cart object
 * @param string $default_language
 *   default language
 */
function euleo_get_action_html($lang_code, $entry, $unsupported, $cart, $default_language = 'de') {
  ob_start();

  $icon_dir = euleo_get_icon_dir();

  $code = 'node_' . $entry->nid;

  $clean_uri = 'translation?action=';

  if (!isset($unsupported[$default_language][$lang_code]) || !$unsupported[$default_language][$lang_code]) {
    if (isset($cart['alreadyOrdered'][$code][$lang_code])) {
      ?>
        <img src="<?php echo $icon_dir; ?>/pending.png" alt="" title="<?php echo t('Translation pending'); ?>" />
      <?php
    }
    else {
      if (isset($cart['request2languages'][$code]) && isset($cart['request2languages'][$code][$lang_code])) {
        ?>
          <a href="<?php echo $clean_uri; ?>remove&amp;code=<?php echo $code; ?>&amp;dstlang=<?php echo $lang_code; ?>">
            <img src="<?php echo $icon_dir; ?>/remove.png" alt="" title="<?php echo t('Remove from cart'); ?>" />
          </a>
        <?php
      }
      else {
        ?>
          <a href="<?php echo $clean_uri; ?>add&amp;code=<?php echo $code; ?>&amp;dstlang=<?php echo $lang_code; ?>">
            <img src="<?php echo $icon_dir; ?>/add.png" alt="" title="<?php echo t('Add to cart'); ?>" />
          </a>
        <?php
      }
    }
  }
  else {
    ?>
      <img src="<?php echo $icon_dir; ?>/unsupported.png" alt="" title="<?php echo t('Language unsupported'); ?>" />
    <?php
  }

  return ob_get_clean();
}

/**
 * Returns the icon dir for HTML.
 *
 * @return string
 *   Icon directory
 */
function euleo_get_icon_dir() {
  return base_path() . drupal_get_path('module', 'euleo') . '/images';
}

/**
 * Returns the translation state.
 *
 * @param object $node
 *   Node object in default language
 * @param object $translation_node
 *   Node in translated language
 * @param string $language
 *   language in ISO-631-1
 *
 * @return string
 *   status
 */
function euleo_get_translation_state($node, $translation_node = NULL, $language = '') {
  if ($language == $node->language) {
    return 'original';
  }

  // Determine the status of the translation.
  if (!empty($translation_node->nid)) {
    if ($translation_node->translate) {
      return 'outofdate';
    }
    return 'current';
  }

  return 'missing';
}
