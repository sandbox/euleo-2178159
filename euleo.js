function euleo_checkAll(className, checkbox) {
  var checkboxes = document.getElementsByClassName(className);
  for (var i = 0; i < checkboxes.length; i++) {
    checkboxes[i].checked = checkbox.checked;
  }
}
