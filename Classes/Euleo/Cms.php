<?php
/**
 * @file
 * class Euleo_Cms
 */

/**
 * link to the euleo soap api
 */
class EuleoCms {
  protected $client;

  protected $usercode;

  protected $handle;

  protected $clientVersion = 2.0;

  protected $cms = '';

  /**
   * Constructor.
   *
   * @param string $customer
   *   customer identification. eg. ci1337
   * @param string $usercode
   *   usercode for authentication
   * @param string $cms
   *   name of the CMS
   *
   * @throws Exception
   */
  public function __construct($customer, $usercode, $cms) {
    $this->customer = $customer;
    $this->usercode = $usercode;

    $this->cms = $cms;

    if ($_SERVER['HTTP_HOST'] == 'ianus' || $_SERVER['SERVER_ADDR'] == '192.168.1.10') {
      $this->host = "http://euleo/";
    }
    else {
      $this->host = "https://www.euleo.com/";
    }

    $this->client = new SoapClient($this->host . '/soap/index?wsdl=1');

    if (!empty($customer) && !empty($usercode)) {
      $request = array();
      $request['customer'] = $this->customer;
      $request['usercode'] = $this->usercode;
      $request['clientVersion'] = $this->clientVersion;

      $request_xml = $this->createRequest($request, 'connect');

      $response_xml = $this->client->__soapCall('connect', array(
        'xml' => $request_xml,
      ));

      $response = $this->parseXml($response_xml);

      if (isset($response['handle']) && $response['handle']) {
        $this->handle = $response['handle'];
      }
      else {
        $this->handle = FALSE;

        throw new Exception($response['errors']);
      }
    }
  }

  /**
   * Returns the connection status.
   * 
   * @return bool
   *   Connection status
   */
  public function connected() {
    return $this->handle != FALSE;
  }

  /**
   * Set the list of destination languages.
   *
   * @param string $language_list
   *   List of languages in ISO-631-1
   *
   * @return array
   *   response from Euleo
   */
  public function setLanguageList($language_list) {
    $request = array();
    $request['handle'] = $this->handle;
    $request['languageList'] = $language_list;

    $request_xml = $this->createRequest($request, 'setLanguageList');
    $response_xml = $this->client->__soapCall('setLanguageList', array(
      'xml' => $request_xml,
    ));
    $response = $this->parseXml($response_xml);

    return $response;
  }

  /**
   * Generates a register token to use for redirection to Euleo.
   *
   * @param string $cmsroot
   *   Root of your CMS. eg. https://www.euleo.com/
   * @param string $return_url
   *   The URL you will to be redirected after confirming registration
   *
   * @return string
   *   token. This should be stored persistently.
   */
  public function getRegisterToken($cmsroot, $return_url) {
    $request = array();
    $request['clientVersion'] = $this->clientVersion;
    $request['cms'] = $this->cms;
    $request['cmsroot'] = $cmsroot;
    $request['returnUrl'] = $return_url;

    $request_xml = $this->createRequest($request, 'getRegisterToken');

    $response_xml = $this->client->__soapCall('getRegisterToken', array(
      'xml' => $request_xml,
    ));

    $response = $this->parseXml($response_xml);

    return $response['token'];
  }

  /**
   * Get customer info by token.
   *
   * @param string $token
   *   Token you have saved after getRegisterToken()
   *
   * @throws Exception
   *
   * @return array
   *   data of the token: customer, usercode...
   */
  public function install($token) {
    $request = array();
    $request['token'] = $token;

    $request_xml = $this->createRequest($request, 'getTokenInfo');

    $response_xml = $this->client->__soapCall('getTokenInfo', array(
      'xml' => $request_xml,
    ));

    $response = $this->parseXml($response_xml);

    if (empty($response['data'])) {
      throw new Exception($response['errors']);
    }

    return $response['data'];
  }

  /**
   * Get customer info by handle.
   *
   * @throws Exception
   *
   * @return array
   *   data of the customer: emailaddress, name...
   */
  public function getCustomerData() {
    $request = array();
    $request['handle'] = $this->handle;
    $request_xml = $this->createRequest($request, 'getCustomerData');

    $response_xml = $this->client->__soapCall('getCustomerData', array(
      'xml' => $request_xml,
    ));

    $response = $this->parseXml($response_xml);

    if (empty($response['data'])) {
      throw new Exception($response['errors']);
    }

    return $response['data'];
  }

  /**
   * Get login link for Euleo.
   *
   * @return string
   *   Login link
   */
  public function startEuleoWebsite() {
    return $this->host . "/business/auth/index/handle/" . $this->handle;
  }

  /**
   * Get new translations.
   *
   * @return array
   *   rows. @see readme.md
   */
  public function getRows() {
    $request = array();
    $request['handle'] = $this->handle;

    $request_xml = $this->createRequest($request, 'getRows');

    $response_xml = $this->client->__soapCall('getRows', array(
      'xml' => $request_xml,
    ));

    $response = $this->parseXml($response_xml);

    if (!$response['rows']) {
      echo 'No translations available at this time.';
    }

    return $response['rows'];
  }

  /**
   * Send rows to Euleo.
   *
   * @param array $rows
   *   rows. @see readme.md
   *
   * @return array
   *   response from Euleo
   */
  public function sendRows($rows) {
    $request = array();
    $request['handle'] = $this->handle;
    $request['cms'] = $this->cms;
    $request['rows'] = $rows;

    $request_xml = $this->createRequest($request, 'sendRows');

    $response_xml = $this->client->__soapCall('sendRows', array(
      'xml' => $request_xml,
    ));

    $response = $this->parseXml($response_xml);

    return $response;
  }

  /**
   * Confirm that the rows have correctly been imported into CMS.
   * 
   * @param array $translationids
   *   the id of a translation is found in the result of sendRows()
   *
   * @return array
   *   response from Euleo
   */
  public function confirmDelivery(array $translationids) {
    $request['handle'] = $this->handle;
    $request['translationIdList'] = implode(',', $translationids);
    $request_xml = $this->createRequest($request, 'confirmDelivery');

    $response_xml = $this->client->__soapCall('confirmDelivery', array(
      'xml' => $request_xml,
    ));

    $response = $this->parseXml($response_xml);

    return $response;
  }

  /**
   * Get the current cart from Euleo.
   *
   * @return array
   *   contents of the cart
   */
  public function getCart() {
    $request['handle'] = $this->handle;
    $request_xml = $this->createRequest($request, 'getCart');

    $response_xml = $this->client->__soapCall('getCart', array(
        'xml' => $request_xml,
    ));
    $response = $this->parseXml($response_xml);

    return $response;
  }

  /**
   * Clear the cart.
   *
   * @return array
   *   response from Euleo
   */
  public function clearCart() {
    $request['handle'] = $this->handle;
    $request_xml = $this->createRequest($request, 'clearCart');

    $response_xml = $this->client->__soapCall('clearCart', array(
        'xml' => $request_xml,
    ));

    $response = $this->parseXml($response_xml);
    return $response;
  }

  /**
   * Add a language to a row.
   *
   * @param string $code
   *   code of the row. eg. articles_1
   * @param string $language
   *   language in ISO-631-1
   *
   * @return array
   *   response from Euleo
   */
  public function addLanguage($code, $language) {
    $request = array();
    $request['handle'] = $this->handle;
    $request['code'] = $code;
    $request['language'] = $language;

    $request_xml = $this->createRequest($request, 'addLanguage');

    $response_xml = $this->client->__soapCall('addLanguage', array(
      'xml' => $request_xml,
    ));

    $response = $this->parseXml($response_xml);
    return $response;
  }

  /**
   * Remove a language from a row.
   *
   * @param string $code
   *   code of the row. eg. articles_1
   * @param string $language
   *   language in ISO-631-1
   *
   * @return array
   *   response from Euleo
   */
  public function removeLanguage($code, $language) {
    $request = array();
    $request['handle'] = $this->handle;
    $request['code'] = $code;
    $request['language'] = $language;

    $request_xml = $this->createRequest($request, 'removeLanguage');

    $response_xml = $this->client->__soapCall('removeLanguage', array(
      'xml' => $request_xml,
    ));

    $response = $this->parseXml($response_xml);

    return $response;
  }

  /**
   * Identify the language of an array of texts.
   *
   * @param array $texts
   *   array of texts to be identified
   *   
   * @return array
   *   response from Euleo
   */
  public function identifyLanguages($texts) {
    $request = array();
    $request['handle'] = $this->handle;
    $request['texts'] = $texts;

    $request_xml = $this->createRequest($request, 'getLanguages');

    $response_xml = $this->client->__soapCall('identifyLanguages', array(
      'xml' => $request_xml,
    ));

    $response = $this->parseXml($response_xml);

    return $response;
  }

  /**
   * Set a callback url.
   *
   * @param string $url
   *   The url which will be called from Euleo.
   *
   * @return array
   *   response from Euleo
   */
  public function setCallbackUrl($url) {
    $request = array();
    $request['handle'] = $this->handle;
    $request['callbackurl'] = $url;

    $request_xml = $this->createRequest($request, 'setCallbackUrl');

    $response_xml = $this->client->__soapCall('setCallbackUrl', array(
      'xml' => $request_xml,
    ));

    $response = $this->parseXml($response_xml);

    return $response;
  }

  /**
   * Creates a request-xml from an array.
   *
   * @param array $data
   *   Request data
   * @param string $action
   *   Name of the action
   *
   * @return string
   *   request as XML
   */
  protected function createRequest($data, $action) {
    if (!is_array($data)) {
      return FALSE;
    }

    $xml = array();
    $xml[] = '<?xml version="1.0" encoding="utf-8" ?>';
    $xml[] = '<request action="' . $action . '">';

    $xml[] = self::createRequestSub($data);

    $xml[] = '</request>';

    $xmlstr = implode("\n", $xml);

    return $xmlstr;
  }

  /**
   * Sub function for createRequest which is called recursively.
   *
   * @param array $data
   *   Request data
   *
   * @return string
   *   request as XML
   */
  protected function createRequestSub($data) {
    $xml = array();

    foreach ($data as $key => $value) {
      if (!is_numeric($key)) {
        $xml[] = '<' . $key . '>';
        if (is_array($value)) {
          $xml[] = self::createRequestSub($value, $key);
        }
        else {
          $xml[] = '<![CDATA[' . trim($value) . ']]>';
        }
        $xml[] = '</' . $key . '>';
      }
      else {
        $xml[] = self::rowToXmlSub($value);
      }
    }

    $xmlstr = implode("\n", $xml);
    return $xmlstr;
  }

  /**
   * Creates row-XML from array.
   *
   * @param array $row
   *   row. @see readme.md
   *
   * @return string
   *   XML
   */
  protected function rowToXmlSub($row) {
    $lines = array();

    if (empty($row['code'])) {
      $row['code'] = '';
    }
    if (empty($row['label'])) {
      $row['label'] = '';
    }
    if (empty($row['title'])) {
      $row['title'] = '';
    }
    if (empty($row['url'])) {
      $row['url'] = '';
    }

    $lines[] = '<row id="' . htmlspecialchars($row['code'], ENT_COMPAT, 'UTF-8') . '" label="' . htmlspecialchars($row['label'], ENT_COMPAT, 'UTF-8') . '" title="' . htmlspecialchars($row['title'], ENT_COMPAT, 'UTF-8') . ($row['url'] ? '" url="' . htmlspecialchars($row['url'], ENT_COMPAT, 'UTF-8') : '') . '">';

    foreach ($row as $key => $value) {
      if ($key != 'fields' && $key != 'rows') {
        $lines[] = '<' . $key . '><![CDATA[' . $value . ']]></' . $key . '>';
      }
    }

    if ($row['fields']) {
      $lines[] = '<fields>';

      foreach ($row['fields'] as $fieldname => $field) {
        $label = $field['label'];

        if ($label == '') {
          $label = ucfirst($fieldname);
        }
        $lines[] = '<field name="' . htmlspecialchars($fieldname) . '" label="' . htmlspecialchars($label) . '" type="' . htmlspecialchars($field['type']) . '">';

        $lines[] = '<![CDATA[';
        $lines[] = $field['value'];
        $lines[] = ']]>';

        $lines[] = '</field>';
      }
      $lines[] = '</fields>';
    }

    if (isset($row['rows'])) {
      $lines[] = '<rows>';
      foreach ($row['rows'] as $childrow) {
        $lines[] = self::rowToXmlSub($childrow);
      }
      $lines[] = '</rows>';
    }

    $lines[] = '</row>';

    $xmlstr = implode("\n", $lines);

    return $xmlstr;
  }

  /**
   * Parse XML to array.
   *
   * @param string $xml
   *   XML string
   *
   * @return array
   *   array
   */
  protected function parseXml($xml) {
    if (!$xml) {
      return FALSE;
    }

    $return = array();

    $sxml = new EuleoSimpleXml();
    $node = $sxml->xmlLoadFile($xml, 'array', 'utf-8');

    $return = self::parseXmlSub($node);

    return $return;
  }

  /**
   * Sub function for parseXml.
   *
   * @param object $rownode
   *   row node
   *
   * @return array
   *   row
   */
  protected function parseXmlSub($rownode) {
    if (isset($rownode['@attributes']) && is_array($rownode['@attributes'])) {
      foreach ($rownode['@attributes'] as $key => $value) {
        $row[$key] = trim((string) $value);
      }
    }

    foreach ($rownode as $name => $child) {
      if ($name != 'fields' && $name != 'rows' && $name != 'requests' && $name != '@attributes') {
        if (is_array($child)) {
          $row[$name] = $child;
        }
        else {
          $row[$name] = trim((string) $child);
        }
      }
    }

    if (isset($rownode['fields']['field']) && $rownode['fields']['field']) {
      if (isset($rownode['fields']['field'][0])) {
        $fields = $rownode['fields']['field'];
      }
      else {
        $fields = array(
          $rownode['fields']['field'],
        );
      }
      foreach ($fields as $fieldnode) {
        $field = array();
        foreach ($fieldnode['@attributes'] as $key => $value) {
          $field[$key] = trim((string) $value);
          if ($key == 'name') {
            $fieldname = trim((string) $value);
          }
        }
        $field['value'] = trim((string) $fieldnode['@content']);
        $row['fields'][$fieldname] = $field;
      }
    }
    if (isset($rownode['rows']) && $rownode['rows']) {
      $row['rows'] = array();

      if (isset($rownode['rows']['row'][0])) {
        $rows = $rownode['rows']['row'];
      }
      else {
        $rows = array(
          $rownode['rows']['row'],
        );
      }

      foreach ($rows as $childrownode) {
        $row['rows'][] = self::parseXmlSub($childrownode);
      }
    }
    if (isset($rownode['requests']) && $rownode['requests']) {
      $row['requests'] = array();

      if (isset($rownode['requests']['request'][0])) {
        $rows = $rownode['requests']['request'];
      }
      else {
        $rows = array(
          $rownode['requests']['request'],
        );
      }

      foreach ($rows as $childrownode) {
        $row['requests'][] = self::parseXmlSub($childrownode);
      }
    }

    return $row;
  }
}
