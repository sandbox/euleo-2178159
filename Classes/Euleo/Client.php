<?php
/**
 * @file
 * class EuleoClient
 */


/**
 * Bridge between Drupal and EuleoCms
 */
class EuleoClient {
  protected $rows = array();

  /**
   * @var Euleo_Cms
   */
  protected $cms = NULL;

  protected $callbackUrl = '';

  /**
   * Adds a row.
   *
   * @param array $row
   *   Row to add
   */
  public function addRow($row) {
    $this->rows[] = $row;
  }

  /**
   * Create a EuleoCms object.
   *
   * @param string $customer
   *   customer identification. eg. ci1337
   * @param string $usercode
   *   usercode for authentication
   *
   * @throws Exception
   */
  public function connect($customer = '', $usercode = '') {
    if (!$customer) {
      $customer = variable_get('euleo_customer');
    }
    if (!$usercode) {
      $usercode = variable_get('euleo_usercode');
    }

    try {
      $this->cms = new EuleoCms($customer, $usercode, 'drupal');
      return $this->cms->connected();
    }
    catch (Exception $e) {
      variable_del('euleo_customer');
      variable_del('euleo_usercode');

      throw $e;
    }
  }

  /**
   * Store the token data persistently.
   *
   * @param string $token
   *   token from Euleo
   */
  public function install($token) {
    $data = $this->cms->install($token);

    variable_set('euleo_customer', $data['customer']);
    variable_set('euleo_usercode', $data['usercode']);

    variable_del('euleo_install_token');
  }

  /**
   * Send rows to Euleo.
   *
   * @throws Exception
   * 
   * @return array
   *   response from Euleo
   */
  public function sendRows() {
    if (!$this->rows) {
      throw new Exception('No rows found. Did you select entries?');
    }

    if (!$this->cms) {
      $this->connect();
    }

    $response = $this->cms->sendRows($this->rows);

    return $response;
  }

  /**
   * Proxy for EuleoCms::startEuleoWebsite.
   *
   * @return string
   *   Login link
   */
  public function startEuleoWebsite() {
    return $this->cms->startEuleoWebsite();
  }

  /**
   * Proxy for EuleoCms::getRows.
   *
   * @return array
   *   rows. @see readme.md
   */
  public function getRows() {
    if (!$this->cms) {
      $this->connect();
    }

    $rows = $this->cms->getRows();

    return $rows;
  }

  /**
   * Proxy for EuleoCms::confirmDelivery.
   *
   * @param array $ids
   *   the id of a translation is found in the result of sendRows()
   *
   * @return array
   *   response from Euleo
   */
  public function confirmDelivery($ids) {
    $this->cms->confirmDelivery($ids);
  }

  /**
   * Proxy for EuleoCms::getCart.
   *
   * @return array
   *   contents of the cart
   */
  public function getCart() {
    return $this->cms->getCart();
  }

  /**
   * Set the list of destination languages.
   *
   * @param array $languages
   *   Array of languages in ISO-631-1
   *
   * @return array
   *   response from Euleo
   */
  public function setLanguages($languages) {
    $language_list = implode(',', array_keys($languages));

    return $this->cms->setLanguageList($language_list);
  }

  /**
   * Proxy for EuleoCms::addLanguage.
   *
   * @param string $code
   *   code of the row. eg. articles_1
   * @param string $language
   *   language in ISO-631-1
   *
   * @return array
   *   response from Euleo
   */
  public function addLanguage($code, $language) {
    return $this->cms->addLanguage($code, $language);
  }

  /**
   * Proxy for EuleoCms::removeLanguage.
   *
   * @param string $code
   *   code of the row. eg. articles_1
   * @param string $language
   *   language in ISO-631-1
   *
   * @return array
   *   response from Euleo
   */
  public function removeLanguage($code, $language) {
    return $this->cms->removeLanguage($code, $language);
  }

  /**
   * Store translated rows in Drupal.
   *
   * @param array $rows
   *   Rows from getRows()
   *
   * @return string
   *   Stored ids to confirm with confirmDelivery()
   */
  public function callback($rows) {
    $confirm_ids = array();

    if (count($rows) > 0) {
      foreach ($rows as $row) {
        if ($row['ready']) {
          $confirm_ids[$row['translationid']] = $row['translationid'];
        }

        list ($what, $id) = explode('_', $row['id']);

        if ($what == 'node') {
          $node = node_load($id);
          $translations = translation_node_get_translations($node->tnid);

          if (isset($translations[$row['lang']])) {
            $translation_node = node_load($translations[$row['lang']]->nid);
          }
          else {
            $tmp = new stdClass();
            $tmp->uid = $node->uid;
            $tmp->type = $node->type;
            $tmp->language = $row['lang'];
            if ($node->tnid == 0) {
              $node->tnid = $node->nid;
              node_save($node);
            }
            $tmp->tnid = $node->tnid;

            node_save($tmp);
            $translation_node = node_load($tmp->nid);
          }

          $translation_node->status = 1;

          foreach ($row['fields'] as $field) {
            if (is_string($node->$field['name'])) {
              $translation_node->$field['name'] = $field['value'];
            }
          }
          foreach ($row['rows'] as $subrow) {
            list ($field_name, $language, $id) = explode('||', $subrow['id']);

            foreach ($subrow['fields'] as $field) {
              $orig_field = $node->$field_name;
              $field['format'] = $orig_field[$language][$id]['format'];

              $tmp = &$translation_node->$field_name;
              $tmp[$language][$id] = $field;
            }
          }

          node_save($translation_node);
        }
      }
    }

    return $confirm_ids;
  }
}
